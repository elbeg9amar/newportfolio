
import getStripe from '../components/stripe/stripe'


export const sendPayment = async uid => {
    const stripe = await getStripe()
    const response = await fetch("https://us-central1-newportfolio-39925.cloudfunctions.net/sendPayments", {
        method:'POST',
        body: uid,
    })
    const session = await response.json()
    const {error} = await stripe.redirectToCheckout({
            sessionId: session.id,
    })
    if(error) {
        console.log(error)
    };
};
