import React from "react";
import ContactCom from '../components/ContactCom';


const Contact = () => (
  <section>
    <ContactCom />
  </section>
);

export default Contact;