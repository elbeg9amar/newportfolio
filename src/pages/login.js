import React,{useContext, useState} from "react";
import { navigate } from "gatsby";
import {FirebaseContext} from '../components/firebase';

import {Form, Button} from '../components/common/form';
import {loginAsUser, loginAsAdmin} from '../hooks/userHook'

const initialState = {
  email:'',
  password:''
};
const Login = () => {
  const [values, setValues] = useState(initialState);
  const {firebase} = useContext(FirebaseContext);
  const [errMessage, setErrMessaga] =useState('');

  function handleChanges(e){
    e.persist();
    setErrMessaga('');
    setValues(currentValues => ({
      ...currentValues,
      [e.target.name]: e.target.value
    }));
  };

  function handleSubmit (e) {
    e.preventDefault();
    firebase.login({email: values.email, 
      password: values.password})
      .then(res => navigate('/'))
      .catch(err => {
        console.log(err);
        setErrMessaga(err.message);
    });
    navigate("/");
  };

  function loginUser() {
    setValues(initialState);
    const {email,password} = loginAsUser()
    firebase.login({email:email, password:password})
      .then(res => navigate('/'))
      .catch(err =>  setErrMessaga(err.message));
  };

  function loginAdmin() {
    setValues(initialState);
    const {email,password} = loginAsAdmin()
    firebase.login({email:email, password:password})
      .then(res => navigate('/'))
      .catch(err => setErrMessaga(err.message));
  };

  return (
    <Form>
      <form onSubmit={handleSubmit}>
      <h1>Login</h1>
      <label>
        <strong>Email</strong>
        <input 
          required
          placeholder="email" 
          type="email" 
          onChange={handleChanges} 
          name="email"
          value={values.email}
        />
      </label>
      
      <label>
        <strong>Password</strong>
        <input 
          required
          placeholder="password" 
          type="password"
          onChange={handleChanges} 
          name="password"
          value={values.password}
          />
      </label>
      {
        !!errMessage && <span style={{color:"red", backgroundColor:"#ddd"}}>{errMessage}</span>
      }
      <Button>
        <button type="submit">Login</button>
        <button onClick={loginUser}>Login As User</button>
        <button onClick={loginAdmin}>Login As Admin</button>
      </Button>
      </form>
    </Form>
  );
};

export default Login;
