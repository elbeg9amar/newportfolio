import React,{useContext, useState} from "react";
import {FirebaseContext} from '../components/firebase';
import {RegisterForm} from '../components/common/form';
import { navigate } from "gatsby"

const initialValue = {
    username:'',
    email:'',
    password:'',
    confirmPassword:''
};
const Register = () => {
    const {firebase} = useContext(FirebaseContext);
    const [formValues,setFormValues] = useState(initialValue);
    const [errMessage, setErrMessaga] = useState('');
    const handleChanges = e => {
        e.persist();
        setFormValues(currentValues => ({
        ...currentValues,
        [e.target.name]: e.target.value
        }));
    };

    const handleSubmit = e => {
        e.preventDefault();
        if(formValues.password !== formValues.confirmPassword){
            setErrMessaga('Password did not match')
            return
        };
        firebase.register({
            email: formValues.email,
            username: formValues.username,
            password: formValues.password
        })
        .then(res => { 
            navigate('/'); 
            setFormValues(initialValue)
        }).catch(err => setErrMessaga(err.message))
    };

    return (
    <RegisterForm>
        <form onSubmit={handleSubmit}>
            <header><h1>Sign up Here</h1></header>

            <label>
                <strong>Username</strong>
                <input type="text"
                name="username"
                placeholder="username"
                value={formValues.username}
                onChange={handleChanges} />
            </label>

            <label>
                <strong>Email</strong>
                <input type="email"
                name="email"
                placeholder="email"
                value={formValues.email}
                onChange={handleChanges} />
            </label>

            <label>
                <strong>Password</strong>
                <input type="password"
                name="password"
                placeholder="password"
                value={formValues.password}
                onChange={handleChanges} />
            </label>

            <label>
                <strong>Confirm Password</strong>
                <input type="password"
                name="confirmPassword"
                placeholder="confirmPassword"
                value={formValues.confirmPassword}
                onChange={handleChanges} />
            </label>
            {
                !!errMessage && <span style={{color:"red", backgroundColor:"#ddd"}}>{errMessage}</span>
            }
            <button>Sign Up</button>
        </form>
    </RegisterForm>
    );
};

export default Register;
