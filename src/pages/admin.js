import React, {useState, useEffect, useContext} from "react";
import styled from 'styled-components';

import {FirebaseContext} from '../components/firebase';
import Messages from '../components/message/Messages';
import ProjectAdd from '../components/projectAdd/ProjectAdd';
import HistoryChart from '../components/HistoryChart';

const Section = styled.section`
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin: 30px auto;
    @media screen and (max-width: 800px) {
        flex-direction: column;
        width: unset;
        align-items: center;
        > *:first-child {
            margin-bottom: 50px;
        }
    }
`

const Admin = () => {
    const [donations, setDonations] = useState();
    const {firebase} = useContext(FirebaseContext);
    useEffect(() => {
        if(firebase){
            firebase.allhistory().then(res => {
                res.subscribe(docs => setDonations(docs))
            })
        }
    },[firebase]);
    
    return (
        <>
        <Section>
            <Messages/>
            <ProjectAdd />
        </Section>
        <h1 style={{textAlign:'center'}}>Donation history</h1>
        {
            !!donations &&
            donations.map(d => (
                <HistoryChart key={d.sessionId} admin="admin" data={d}/>
            ))
        }
        </>
    );
};

export default Admin;


