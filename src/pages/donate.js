import React, {useContext} from "react";
import {Link} from 'gatsby';
import styled from 'styled-components';
import {FirebaseContext} from '../components/firebase';

import Donation from '../components/Donation';
import DonationHistory from '../components/DonationHistory';

import {checkAdmin} from '../hooks/userHook'


const DonateStyled = styled.div`
    text-align: center;
    max-width: 50%;
    margin: 40px auto;
    display:flex;
    justify-content: space-evenly;
    border: black solid 1px;
    margin-bottom: 10px;
    span {
        font-size: 2.5em;
    }
    .login {
        font-size: 2.5em;
        text-decoration:none;
        color:blue;
    }
    .test-warning {
        text-align: center;
        margin-top: 40px;
        font-size: 24px;
        color: red;
    }
`


const Donate = () => {
    const {user} =useContext(FirebaseContext);

 
        return (
            <>
            {
                (!user || !user.email) &&
                    <DonateStyled>
                        <span>
                            Please 
                        </span>
                        
                        <Link to="/login" className="login">
                            Login
                        </Link> 
                    </DonateStyled>
                
            }
            { !!user && !!user.email &&  
                <Donation user={user}/> 
            }
            <h1> History </h1>
            { !!user && !!user.email &&  
                <DonationHistory userId={user.uid}/>
            }
            </>
        )
}

export default Donate
