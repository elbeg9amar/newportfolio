import React from "react";

import Intro from '../components/Intro';
import Exp from '../components/expriences/Exp';

const AboutPage = (props) => {
  const aboutData = props.data.allAbout.edges[0].node;
  const expData = props.data.allExprience.edges.map(exp => (exp.node));

  return (
    <section>
      <Intro aboutData={aboutData} about={'about'}/>
      <Exp expData={expData}/>
    </section>
  );
};

export const quert = graphql`
 {
  allAbout {
    edges {
      node {
        aboutText
        aboutTitle
        id
        name
        position
        localImage {
          publicURL
        }
      }
    }
  }
  allExprience {
    edges {
      node {
        id
        date
        company
        position
        text
      }
    }
  }
}

`;

export default AboutPage;