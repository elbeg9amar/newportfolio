import React from "react";

import Intro from '../components/Intro';
import ProjectCom from '../components/ProjectCom';


const IndexPage = (props) => {
  const aboutData = props.data.allAbout.edges[0].node;
  const projectData = props.data.allProject.edges.map(pro => (pro.node));
  return (
    <>
    <section>
      <Intro aboutData={aboutData}/>
      <ProjectCom projectData={projectData} about={'about'}/>
    </section>
    </>
  );
};

export const quert = graphql`
  {
    allAbout {
      edges {
        node {
          aboutText
          aboutTitle
          name
          localImage {
            publicURL
          }
          position
        }
      }
    }
    allProject(limit: 2) {
      edges {
        node {
          id
          title
          picUrl
          libraries
          github
          description
        }
      }
    }
  }
`;


export default IndexPage;
