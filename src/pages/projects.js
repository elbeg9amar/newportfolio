import React from "react";
import ProjectCom from '../components/ProjectCom';

const Projects = (props) => {
  const projectData = props.data.allProject.edges.map(pro => (pro.node));
  return (
      <>
      <section>
        <ProjectCom projectData={projectData}/>
      </section>
      </>
  );
};

export const query = graphql`
    {
    allProject {
      edges {
        node {
          description
          github
          id
          libraries
          picUrl
          title
        }
      }
    }
  }
`;


export default Projects;