const skillData = [
    {
        name: "React",
        value:90,
        id:56
    },
    {
        name:"JavaScript",
        value:80,
        id:57
    },
    {
        name:"JavaScript",
        value:70,
        id:55
    },
    {
        name:"JavaScript",
        value:60,
        id:59
    },
    {
        name:"JavaScript",
        value:50,
        id:60
    }
];

export default skillData;