import React, { useState, useEffect, useContext } from "react";
import styled from 'styled-components';
import {FirebaseContext} from './firebase';
import HistoryChart from './HistoryChart';

const StyledDiv = styled.div`
    /* width: 100%;
    min-height: 90vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 50px auto 0; */
    .donation-header {
        width: 100%;
        padding: 10px 0;
        display: flex;
        justify-content: space-between;
        border-bottom: 1px solid darkgrey;
        .inputs {
        text-transform: capitalize;
        width: 23%;
        &:last-child {
            width: 8%;
            }
        }
    }
`

const DonationHistory = ({userId, admin}) => {

    const [donations, setDonations] = useState();
    const {firebase} = useContext(FirebaseContext);
    useEffect(() => {
        if(firebase){
            firebase.history(userId).then(res => {
                res.subscribe(docs => setDonations(docs))
            })
        }
    },[firebase,userId]);
    return (
        <>
        <StyledDiv >
            <div className="donation-header">
                <div className="inputs">
                    <span>Date</span>
                </div>
                {
                    !!admin &&
                    <div className="inputs">
                        <span>Name</span>
                    </div>
                }

                <div className="inputs">
                    <span>Amount</span>
                </div>

                <div className="inputs">
                    <span>sessionId</span>
                </div>
            </div>
            {
                !donations && 
                <div>
                    <p>No history</p>
                </div>
            }
            {   !!donations &&
                donations.map(d => (
                    <HistoryChart key={d.sessionId} data={d}/>
                ))
                
            }

        </StyledDiv>
        </>
    );
};

export default DonationHistory;



