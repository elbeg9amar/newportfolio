import styled from "styled-components"

export const ExpContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .title {
    text-align: center;
  }
`

export const ExpBox = styled.div`
  font-size: 12;
  font-family: "Roboto", sans-serif;
  width: 80%;
  p {
    font-family: "Roboto", sans-serif;
  }

  .info {
    width: 30%;

    @media screen and (max-width: 900px) {
      width: 70%;
    }
  }

  .details {
    width: 70%;
  }

  .date {
    font-size: 12px;
    font-style: italic;
  }

  .exp {
    display: flex;
    justify-content: space-between;
    align-items: center;

    @media screen and (max-width: 900px) {
      flex-direction: column;
    }
  }

  @media screen and (max-width: 900px) {
    width: 100%;
  }
`