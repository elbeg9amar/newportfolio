import styled from 'styled-components';

export const Form = styled.section`
    font-family:sans-serif;
    background-image: url('https://images.unsplash.com/photo-1584433144859-1fc3ab64a957?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1488&q=80');
    background-size:cover;
    display:flex;
    background-size: cover;
    justify-content:center;
    align-items:center;
    height:100vh;
    form {
        position: absolute;
        top:65%;
        left:50%;
        align-items:center;
        transform: translate(-50%,-50%);
        width:400px;
        background: rgba(0,0,0,0.8);
        box-sizing:border-box;
        box-shadow: 0 15px 25px rgba(0,0,0,0.5);
        border-radius:10px;
    }
    h1 {
        color: #fff;
        text-align: center;
    }
    strong{
        color: #fff;
        text-align: center;
        font-size: 18px;
        padding-left:5%;
    }
    input {
        width:100%;
        padding: 10px 0 ;
        font-size: 16px;
        color: #fff;
        margin-bottom:30px;
        border: none;
        border-bottom: 1px solid #fff;
        outline:none;
        background: transparent;
    }
    button {
        background: transparent;
        border:none;
        outline:none;
        color: #fff;
        background: #03a9f4;
        padding: 10px 20px;
        cursor: pointer;
        border-radius:5px;
        &:hover{
        background-color:#ddd;
        }
    }
    span{
        margin-bottom:2%;
        color:red;
    }
`;

export const Button = styled.div`
    display:flex;
    flex-direction:column;
    flex-wrap:wrap;
`;

export const RegisterForm = styled.section`
    font-family: sans-serif;
    background-image: url('https://images.unsplash.com/photo-1522709553816-36115b3cc2ef?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80');
    display:flex;
    background-size: cover;
    justify-content:center;
    align-items:center;
    height:100vh;
    form {
        max-width:350px;
        border-radius:20px;
        margin:auto;
        background: rgba(0,0,0,0.8);
        box-sizing:border-box;
        padding:40px;
        color: #fff;
        h1{
            text-align:center;
        }
        input{
            width:100%;
            box-sizing: border-box;
            padding: 12px 5px;
            background: rgba(0,0,0,0.10);
            outline: none;
            border:none;
            border-bottom: 1px dotted #fff;
            color:#fff;
            border-radius:5px;
            margin:5px;
            font-weight:bold;
            margin-top: 2%;
        }
        button {
            width:100%;
            box-sizing:border-box;
            padding:10px 0;
            margin-top:30px;
            outline:none;
            border:none;
            background: #60adde;
            opacity:0.7;
            border-radius:20px;
            font-size:20px;
            color:#fff;
            &:hover{
                background-color:blue;
            }
        }
    }
`;