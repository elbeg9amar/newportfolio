import React from "react";

import {FirebaseContext, useAuth} from './firebase';

import Header from "./Header";
import Footer from './Footer';
import "./layout.css";

const Layout = ({ children }) => {
  const {user,firebase,loading} = useAuth();

  return (
    <FirebaseContext.Provider value={{user,loading,firebase}}>
      <Header />
        <main>{children}</main>
      <Footer />
    </FirebaseContext.Provider>
  );
};

export default Layout;
