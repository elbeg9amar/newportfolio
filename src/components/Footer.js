import React from 'react';
import SocialLinks from '../dummyData/socialLinks';

import {
    FooterStyled,
    FooterContainer
} from './styles/footer.styled';


function Footer() {
    return (
        <FooterStyled>
            <FooterContainer>
                <SocialLinks/>
                <h4>
                    copyright&copy;{new Date().getFullYear()}
                    <span> Elbeg </span> all rights reserved
                </h4>
            </FooterContainer>
        </FooterStyled>
    );
};

export default Footer;
