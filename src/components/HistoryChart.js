import React from 'react';


import styled from 'styled-components';

const StyledDiv = styled.div`
    width: 100%;
    display: flex;
    min-height: 100px;
    border-bottom: 1px solid darkgrey;
    padding: 15px 0;
    font-size: 20px;
    align-items: center;
    justify-content: space-between;
    .date,
    .amount,
    .message
    .userId {
        width: 0%;
    }
`

const HistoryChart = ({data,admin}) => {
    const timeStamp = data.createdAt;
    const time = new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(timeStamp)
    return (
        <StyledDiv> 
            <span className="date">{time}</span>
            <span className="amount">10$</span>
            <span className="userId" style={{width:"25%", height:"10%"}}>{data.sessionId.toString()}</span>

        </StyledDiv>
    )
}

export default HistoryChart;


