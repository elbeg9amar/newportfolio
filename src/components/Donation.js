
import React, { useState } from "react";
import { Link} from "gatsby";
import {sendPayment} from '../hooks/stripeHook';



const Donation = ({user}) => {
    const [url,setUrl] = useState('');
    const redirectToCheckout  = async e => {
        e.preventDefault();
        sendPayment(user.uid)
    };

    const cryptoDonation = async e => {
        e.preventDefault();
        const res = await fetch('https://us-central1-newportfolio-39925.cloudfunctions.net/createCharge', {
            method:'POST',
            body: user.uid,
            }   
        );
        const data = await res.json();
        const hosted_url = data.hosted_url;
        setUrl(hosted_url);
    };

    const checkStatus = async e => {
        e.preventDefault();
         const statusRes = await fetch('https://us-central1-newportfolio-39925.cloudfunctions.net/coinbaseWebHookHandler')
        console.log(statusRes)
    }

    return (
        <div>
            <button onClick={redirectToCheckout}> 10$ </button>
            <button onClick={cryptoDonation}> Crypto </button>
            {
                !!url &&
                <Link to={url}> To Coinbase </Link>
            }
            <button onClick={checkStatus}> Payment Status</button>
        </div>
        
    );
};

export default Donation;

