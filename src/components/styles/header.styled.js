import styled from 'styled-components';
import {Link} from 'gatsby';




export const HeaderContainer = styled.div`
    border: 5px #ddd;
    height: 70px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin-bottom: 10px;

    @media screen and (max-width:800px) {
        padding: 10px;
        height:60px;
        margin-bottom:20px;
    }
`;

export const LogoContianer = styled(Link)`
    img {
        height: 100%;
        width: 70px;
        /* padding: 25px; */
        @media screen and (max-width:800px) {
            padding: 0;
            width:50px;
    }
    }
    
`;

export const OptionsContainer = styled.div`
    width: 60%;
    height: 100%;
    display: flex;
    a {
        text-decoration:none;
        color:black;
        &:hover{
            text-decoration:underline;
        }
    }
    align-items: center;
    justify-content: space-evenly;
    @media screen and (max-width:800px) {
        width:80%;
    }
`;

export const OptionLink = styled(Link)`
    /* padding: 10px 15px; */
    cursor: pointer;
`;

export const UserInfo = styled.div`
    span {
        &:hover{
            text-decoration:underline;
            cursor:pointer;
        }
        text-align:right;
        padding-left:45%;
    }
`;

export const LoginDiv = styled.div`
    display:flex;
    justify-content:space-evenly;
    width:25%;
`;
