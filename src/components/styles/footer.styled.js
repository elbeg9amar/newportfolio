import styled from 'styled-components';

export const FooterStyled = styled.footer`
    background:#111;
    height:auto;
    width:100vw;
    font-family:"Open Sans";
    padding-top:40px;
    color:#fff;
`;

export const FooterContainer = styled.div`
    padding-top:1rem;
    text-align: center;
    display: grid;
    place-items: center;
    h4 {
        margin-top: 0.5rem;
        color: var(--clr-white);
        font-weight: normal;
        text-transform: uppercase;
    }
    h4 span {
        color: var(--clr-primary-5);
    }
    ul{
        margin: 0;
        padding: 0;
    }
    li{
        display: inline-block;
        margin: 1px;
        list-style: none;
    }
    li a{
        color: white;
        text-decoration: none;
        font-size: 60px;
        transition: all ease-in-out 250ms;
    }
    li a:hover{
        color: #b9b9b9;
    }
`