import styled from "styled-components"

export const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    border: 1px solid #ddd;
    margin-bottom:15px;
`

export const ProContainer = styled.div`
    text-align: center;
    margin: 2%;
    width: 400px;
    border: 1px solid #ddd;
    img {
        max-width: 100%;
    }
    a {
        color: var(--clr-primary-5);
        font-size: 1.25rem;
        margin-right: 0.5rem;
        transition: var(--transition);
    }
    button {
        min-width: 165px;
        width: auto;
        height: 50px;
        background-color:white;
        margin-bottom:10px;
        &:hover {
            text-transform:uppercase;
            background-color: #ddd;
        }
    }
`

export const Title = styled.h1`
    text-align: center;
`