import firebaseConfig from "./config";
import app from "firebase/app";
import {collectionData} from 'rxfire/firestore'
import { from } from 'rxjs';
import {map} from 'rxjs/operators';
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/storage";

app.initializeApp(firebaseConfig);

class Firebase {
    constructor() {
        if (!firebaseInstance) {
            this.auth = app.auth();
            this.db = app.firestore();
            this.functions = app.functions();
            this.storage = app.storage();
        };
    }

    getUserProfile({userId, onSnapshot}){
        return this.db.collection('publicProfiles')
        .where('userId', "==", userId)
        .get()
    }

    async register({email, password, username}) {
        const newUser = await this.auth.createUserWithEmailAndPassword(email,password);
        // const createProfileCallable = this.functions.httpsCallable('createPublicProfile');
        // return createProfileCallable({
        //     username
        // })
        return this.db.collection('publicProfiles').doc(username).set({
            userId: newUser.user.uid
        })
    }

    async history(id){
        const ref = this.db.collection('donations').orderBy('createdAt','desc');
        const donations$ = from(collectionData(ref));
        return donations$.pipe(
            map(
                results => results.filter(r => r.userId === id)
            )
        );
    };

    async allhistory(){
        const ref = this.db.collection('donations').orderBy('createdAt','desc');
        const donations$ = collectionData(ref);
        return donations$;
    }


    async createProject({title, github, picUrl, description}){
        const createProjectCallable = this.functions.httpsCallable('createProject');
        return createProjectCallable({
            title,
            github,
            picUrl,
            description
        })
    }

    async login({ email, password }) {
        return this.auth.signInWithEmailAndPassword(email, password);
    }

    async postMessages({message,firstName, lastName, email, subject}){
        const postMessagesCallable = this.functions.httpsCallable('PostMessage')
        return postMessagesCallable({
            message,
            firstName,
            lastName,
            email,
            subject,
        });
    }


    async subscribeMessages(){
        return this.db.collection('messages').orderBy('dateCreated','desc').get();
    }

    async logout() {
        await this.auth.signOut();
    }
}

let firebaseInstance;

function getFirebaseInstance() {
    if (!firebaseInstance) {
        firebaseInstance = new Firebase();
        return firebaseInstance;
    } else if (firebaseInstance) {
        return firebaseInstance;
    } else {
        return null;
    }
}

export default getFirebaseInstance;
