import { Link,navigate } from "gatsby";
import React, {useContext} from "react";

import {FirebaseContext} from './firebase';
import logo from '../images/logo.jpg';

import {checkAdmin} from '../hooks/userHook'

import {
  HeaderContainer, 
  LogoContianer, 
  OptionsContainer, 
  OptionLink,
  UserInfo,
  LoginDiv
} from './styles/header.styled';

const Header = () => {
  const {firebase,user} =useContext(FirebaseContext);
  
  function handleLogout() {
    firebase.logout().then(() => navigate('/login'));
  };

  return (
    <>
    <HeaderContainer>
      <LogoContianer to="/">
          <img src={logo} alt="logo" className="logo"/>
      </LogoContianer>
      <OptionsContainer>
          <OptionLink to="/">
              Home
          </OptionLink>
          <OptionLink to="/about">
              About
          </OptionLink>
          <OptionLink to="/projects">
              Projects
          </OptionLink>
          <OptionLink to="/contact">
              Contact
          </OptionLink>
          <OptionLink to="/donate">
              Donate
          </OptionLink>
          {
            !!user && !!user.email && checkAdmin(user.uid) &&
              <OptionLink to="/admin">
                Admin
              </OptionLink>
          }
          <div style={{margin:"auto 0"}}>
            {!!user && !!user.email &&
              <div>
                Hello, {user.username || user.email}
                <UserInfo>
                  <span onClick={handleLogout}>
                    Logout
                  </span>
                </UserInfo>
              </div>
            }
          </div>
          {
            (!user || !user.email) &&
              <LoginDiv>
                <Link to="/login">
                Login
                </Link>
                <Link to="/register">
                Register
                </Link>
              </LoginDiv>
          }
      </OptionsContainer>
    </HeaderContainer>
    </>
  );
};

export default Header;

