import { loadStripe } from "@stripe/stripe-js"
let stripePromise
const getStripe = () => {
    if (!stripePromise) {
        stripePromise = loadStripe("pk_test_51HxvTmK2qDEiyLXyJHRIRZrEPHbMVXBLi7bRCTH8ZFxJKMn7cxYHzSlJUAyfDb0atHcYAIGFa9IpjP10HFPh9SOc00d2YiG188")
    }
    return stripePromise
};

export default getStripe;