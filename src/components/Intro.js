import React from 'react';
import {Link} from "gatsby";

// import skillData from '../../dummyData/skills.data';
import {
    IntroBox,
    IntroContainer,
} from './styles/intro.styled';

function Intro({aboutData, about}) {

    const {name, localImage,position,aboutTitle,aboutText} = aboutData;

    if (!about) {
        return (
            <IntroBox>
                <IntroContainer>
                    <img src={localImage.publicURL} alt="pic" className="img"/>
                    <h1>{name}</h1>
                    <h5>{position}</h5>
                    <Link to="contact" className="btn">
                        Contact me 
                    </Link>
                </IntroContainer>
            </IntroBox>
        );
    }
    else {
        return (
            <IntroBox>
                <IntroContainer>
                    <h1>{aboutTitle}</h1>
                    <img src={localImage.publicURL} alt="pic" className="img"/>
                    <h1>{name}</h1>
                    <h5>{position}</h5>
                    <p>{aboutText}</p>
                </IntroContainer>
            </IntroBox>
        );
    };
};


// {skillData.filter((idx,skill) => idx < 3)
//     .map((skill) => {
//     return  <div key={skill.id}>
//             <h3>{skill.name}</h3>
//             <h4>{skill.value}</h4>
//             </div>
// })}

export default Intro;
