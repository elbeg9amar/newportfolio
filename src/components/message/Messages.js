import React,{useEffect, useContext, useState}  from 'react';
import {FirebaseContext} from '../firebase';

import {
    Sections,
    Div
} from './Messages.styled';
function Messages() {
    const {firebase} = useContext(FirebaseContext)
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        if(firebase){
            firebase.subscribeMessages().then(snapshot => {
                const getMessages = [];
                snapshot.forEach(doc => {
                    getMessages.push({
                        id:doc.id,
                        ...doc.data()
                    })
                })
            setMessages(getMessages)
            })
        }
    },[firebase]);
    
    if(messages){
        return (
            <>
                <Sections>
                <h1> Messages</h1>
                {
                    messages.map(mess => (
                        <Div key={mess.id}>
                            <h3>Name:{mess.firstName}</h3>
                            <h3>Email:{mess.email}</h3>
                            <p>Message:{mess.message}</p>
                        </Div>
                    ))
                }
                </Sections>
            </>
        );
    }
   
};

export default Messages;
