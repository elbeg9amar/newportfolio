import styled from 'styled-components';

export const Sections = styled.section`
    width:48%;
    height:auto;
    h1 {
        text-align:center;
    };
    border: 3px black;
    border-style:solid;
`;

export const Div = styled.div`
    border:3px solid #ddd;
    padding-left:7px;
    @media screen and (min-width: 800px) {
        width:auto;
    }
`