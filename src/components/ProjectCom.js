import React from "react";
import { FaGithubSquare, FaShareSquare } from "react-icons/fa";
import { Link } from "gatsby";

import { ProContainer, Container, Title } from "../components/styles/projectCom.styled";

const ProjectCom = ({ projectData, about }) => {
    return (
        <>
            <Title>Projects</Title>
            <Container>
            {projectData.map(pro => (
                <ProContainer key={pro.id}>
                    <div className="info">
                    <h2 className="title">{pro.title}</h2>
    
                    <a className="github" href={pro.github}>
                        <FaGithubSquare />
                    </a>
                    <a className="imgUrl" href={pro.picUrl}>
                        <FaShareSquare />
                    </a>
                    <p className="description">{pro.description}</p>
                    </div>
    
                    <div className="image">
                    <img src={pro.picUrl} alt="picture" />
                    </div>
                    {
                        about ? <Link to='/projects'><button> More </button></Link> : ''
                    }
                </ProContainer>
                )
            )}
            </Container>
        </>
    );
};

export default ProjectCom;
