import styled from 'styled-components';

export const Section= styled.section`
    width:48%;
    border: 3px solid black;
    h1{
        text-align:center;
    }
    height:480px;
`;

export const Form = styled.form`
        max-width:auto;
        border-radius:20px;
        margin:auto;
        box-sizing:border-box;
        padding:5px;
        color: #fff;
        input{
            width:97%;
            box-sizing: border-box;
            padding: 12px 5px;
            background: rgba(0,0,0,0.10);
            outline: none;
            border:none;
            border-bottom: 1px dotted #fff;
            color:black;
            border-radius:5px;
            margin:5px;
            font-weight:bold;
            margin-top: 2%;
        }
        button {
            width:100%;
            box-sizing:border-box;
            padding:10px 0;
            margin-top:30px;
            outline:none;
            border:none;
            background: #60adde;
            opacity:0.7;
            border-radius:20px;
            font-size:20px;
            color:#fff;
            &:hover{
                background-color:blue;
            }
        }
`;

export const Span = styled.span`
    color:black;
    text-align:center
`;
