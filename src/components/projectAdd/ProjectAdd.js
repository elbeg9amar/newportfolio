import React, { useState, useContext } from 'react';
import {FirebaseContext} from '../firebase';

import {
    Section,
    Form,
    Span
} from './ProjectAdd.styled'
const initialValue = {
    title:'',
    github:'',
    picUrl:'',
    description:''
}
function ProjectAdd() {
    const {firebase} = useContext(FirebaseContext);
    const [formValues, setFormValues] = useState(initialValue);
    const [success,setSuccess] = useState(false)
    const handleChanges = e => {
        e.persist();
        setFormValues(currentValues => ({
        ...currentValues,
        [e.target.name]: e.target.value
        }));
        setSuccess(false)
    };

    const Submitted = e => {
        e.preventDefault();
        firebase.createProject({
            title: formValues.title,
            github: formValues.github,
            picUrl: formValues.picUrl,
            description: formValues.description
        }).then(res => {
            setSuccess(true);
            setFormValues(initialValue)
        })
        .catch(err => console.log(err))
    }

    return (
        <>
        <Section>
            <h1>Add Projects</h1>
            <Form onSubmit={Submitted}>
            <label>
                <strong>title</strong>
                <input type="text"
                name="title"
                placeholder="title"
                value={formValues.title}
                onChange={handleChanges} />
            </label>

            <label>
                <strong>github</strong>
                <input type="text"
                name="github"
                placeholder="github"
                value={formValues.github}
                onChange={handleChanges} />
            </label>

            <label>
                <strong>picUrl</strong>
                <input type="text"
                name="picUrl"
                placeholder="picUrl"
                value={formValues.picUrl}
                onChange={handleChanges} />
            </label>

            <label>
                <strong>description</strong>
                <input type="description"
                name="description"
                placeholder="description"
                value={formValues.description}
                onChange={handleChanges} />
            </label>
            {
                !!success &&
                <Span>
                    Project Added Successfully
                </Span>
            }
            <button >Submit</button>
            </Form>
        </Section>
        </>
    )
}

export default ProjectAdd;
