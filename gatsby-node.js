const path = require('path')

exports.onCreateWebpackConfig = ({ actions, stage }) => {
        if (stage === "develop-html" || stage === "build-html") {
        actions.setWebpackConfig({
            resolve: {
            mainFields: ["main"],
            },
        })
        } else {
        actions.setWebpackConfig({
            resolve: {
            mainFields: ["browser", "module", "main"],
            },
        })
        }
}
// exports.createPages=({graphql, actions}) => {
//     const {createPage} = actions;
//     const projectPage = path.resolve('src/pages/projects.js')
//     return graphql(`
//         {
//             allProject {
//             edges {
//                 node {
//                 description
//                 github
//                 id
//                 libraries
//                 picUrl
//                 title
//                 }
//             }
//             }
//         }
//     `).then((res) => {
//         if(res.errors){
//             throw res.errors
//         }

//         createPage({
//             path:'/projects',
//             component: projectPage,
//             context:res
//         })
//     });
// };

// exports.createPages = ({graphql, actions}) => {
//     const {createPage} = actions;
//     const aboutPage = path.resolve('src/pages/about.js')
//     return graphql(`
//     {
//             allAbout {
//             edges {
//                 node {
//                 aboutText
//                 aboutTitle
//                 id
//                 name
//                 position
//                 localImage {
//                     publicURL
//                 }
//                 }
//             }
//             }
//             allExprience {
//             edges {
//                 node {
//                 date
//                 company
//                 position
//                 text
//                 }
//             }
//             }
//         }
//     `).then((res) => {
//         if(res.errors){
//             throw res.errors
//         }
//         const aboutData = res.data.allAbout.edges[0].node;
//         const expData = res.data.allExprience.edges.map(exp => (exp.node));
//         const allData = {aboutData, expData};
//         createPage({
//             path: '/about',
//             component: aboutPage,
//             context: allData
//         });
//     });
// };