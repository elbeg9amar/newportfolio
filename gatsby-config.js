require('dotenv').config({
  path: '.env'
})
module.exports = {
  siteMetadata: {
    title: `Gatsby Default Starter`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-remote-images`,
      options: {
        nodeType: 'About',
        imagePath: 'picUrl',
      },
    },
    {
      resolve:'gatsby-firesource',
      options: {
        credential: {
          "type": process.env.FIREBASE_TYPE,
          "project_id": process.env.FIREBASE_PROJECT_ID,
          "private_key_id": process.env.FIREBASE_PRIVATE_KEY_ID,
          "private_key": process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
          "client_email": process.env.FIREBASE_CLIENT_EMAIL,
          "client_id": process.env.FIREBASE_CLIENT_ID,
          "auth_uri": process.env.FIREBASE_AUTH_URI,
          "token_uri": process.env.FIREBASE_TOKEN_URI,
          "auth_provider_x509_cert_url": process.env.FIREBASE_AUTH_PROVIDER_X509_CERT_URL,
          "client_x509_cert_url": process.env.FIREBASE_CLIENT_X509_CERT_URL
        },
        types: [
          {
            type:'About',
            collection:'abouts',
            map: doc=> ({
              aboutText: doc.aboutText,
              aboutTitle: doc.aboutTitle,
              name: doc.name,
              picUrl: doc.picUrl,
              position: doc.position,
              logo:doc.logo
            })
          },
          {
            type:'Project',
            collection:'projects',
            map: doc=> ({
              description: doc.description,
              github: doc.github,
              title: doc.title,
              picUrl: doc.picUrl,
              libraries: doc.libraries,
            })
          },
          {
            type:'Exprience',
            collection:'expriences',
            map: doc=> ({
              company: doc.company,
              date: doc.date,
              position: doc.position,
              text: doc.text,
            })
          },
          {
            type:'Picture',
            collection:'pictures',
            map: doc=> ({
              contactPic: doc.contactPic,
              introPic: doc.introPic,
              loginPic: doc.loginPic,
              regisPic: doc.regisPic,
              logoPic: doc.logoPic,
            })
          },
        ]
      }
    },

    `gatsby-plugin-gatsby-cloud`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
